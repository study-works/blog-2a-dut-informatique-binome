<?php

class CommentGateway {
	private $con;
	private $formatDate = "d/m/Y H\hi";

	public function __construct(Connection $con) {
		$this->con = $con;
	}

	public function findAllCommentsByNewsID(int $newsID) : array {
		$query = "SELECT * FROM b_comment WHERE newsID=:newsID";

		$this->con->executeQuery($query, array(
			':newsID' => array($newsID, PDO::PARAM_INT)
		));

		$results = $this->con->getResults();
		$listComment=array();
		Foreach ($results as $comment)
			$listComment[] = new Comment($comment['id'], $comment['pseudo'], date($this->formatDate, strtotime($comment['date'])), $comment['content'], $comment['newsID']);
		return $listComment;
	}

	public function insertComment(string $pseudo, string $content, int $newsID) {
		$query = "INSERT INTO b_comment VALUES(NULL, :pseudo, LOCALTIME(), :content, :newsID)";

		$this->con->executeQuery($query, array(
			':pseudo' => array($pseudo, PDO::PARAM_STR),
			':content' => array($content, PDO::PARAM_STR),
			':newsID' => array($newsID, PDO::PARAM_INT)
		));
	}

	public function findNbComment() : int {
		$query = "SELECT * FROM b_comment";

		$this->con->executeQuery($query, array());

		$results = $this->con->getResults();

		return count($results);
	}
}