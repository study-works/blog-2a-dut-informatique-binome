<?php

class NewsModel {

	static function getAllNews(int $limit, int $page) : array {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findAllNews($limit, $page);
	}

	static function getAllNewsPublished(int $limit, int $page) {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findAllNewsPublished($limit, $page);
	}

	static function getNewsById(int $ID) : News {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNewsById($ID);
	}

	static function getNewsPublishedById(int $ID) {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNewsPublishedById($ID);
	}

	static function getNewsByTitleContent(string $keyWord) : array {
		global $dsn, $user, $password;

		$str = str_replace(" ", "%", $keyWord);

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNewsByTitleContent($str);
	}

	static function getNewsPublishedByTitleContent(string $keyWord) : array {
		global $dsn, $user, $password;

		$str = str_replace(" ", "%", $keyWord);

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNewsPublishedByTitleContent($str);
	}

	static function getNbNews() : int {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNbNews();
	}

	static function getNbNewsPublished() {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		return $newsG->findNbNewsPublished();
	}

	static function insertNews(string $title, string $image, string $content, bool $published) {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		$newsG->insertNews($title, $image, $content, $published);
	}

	static function deleteNews(int $newsID) {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		$newsG->deleteNews($newsID);
	}

	static function modifyNews(int $id, string $title, string $image, string $content, bool $published) {
		global $dsn, $user, $password;

		$newsG = new NewsGateway(new Connection($dsn, $user, $password));

		$newsG->updateNews($id, $title, $image, $content, $published);
	}
}
