<?php

class CommentModel {

	static function getAllCommentsByNewsID(int $newsID) : array {
		global $dsn, $user, $password;

		$commentG = new CommentGateway(new Connection($dsn, $user, $password));

		return $commentG->findAllCommentsByNewsID($newsID);
	}

	static function insertComment(string $pseudo, string $content, int $newsID) {
		global $dsn, $user, $password;

		$commentG = new CommentGateway(new Connection($dsn, $user, $password));

		$commentG->insertComment($pseudo, $content, $newsID);
	}

	static function getNbComment() : int {
		global $dsn, $user, $password;

		$commentG = new CommentGateway(new Connection($dsn, $user, $password));

		return $commentG->findNbComment();
	}
}
