<section class="copyright">
	<p>Copyright &copy; 2019-2020
		<br />Développé par <a href="mailto:jules.bonfilloup@etu.uca.fr">Jules BONFILLOUP</a> et <a href="mailto:gaethan.brugiere@etu.uca.fr">Gaëthan BRUGIÈRE</a>
	</p>
</section>
