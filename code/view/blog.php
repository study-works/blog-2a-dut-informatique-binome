<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include 'includes/meta.php'; ?>

        <title>Blog | ProgWeb</title>
    </head>

    <body>
		<?php
			// Vérification des données provenant du modèle
			if (isset($listNews))
			{
		?>
		<div id="particles-js"></div>

        <header>
            <?php include 'includes/header.php'; ?>
		</header>

        <main>
			<section class="d-flex justify-content-center flex-wrap">
				<?php
					foreach ($listNews as $news) {
				?>
				<div class="card mt-3 mb-3 ml-3 mr-3" style="width: 18rem;">
					<img src="<?= $news->getImage()?>" class="card-img-top">
					<div class="card-body">
						<h5 class="card-title"><?= $news->getTitle()?></h5>
						<p class="card-text"><?= substr($news->getContent(), 0, 100)?>...</p>
						<a href="?action=news&newsID=<?= $news->getId()?>"><button class="btn btn-primary" type="submit">Lire plus</button></a>
					</div>
					<div class="card-footer text-muted">
						<?= $news->getDate()?>
					</div>
				</div>
				<?php
					}
				?>
			</section>
			<?php
				if (isset($page))
				{
			?>
			<section>
				<nav>
					<ul class="pagination justify-content-center">
						<li class="page-item <?php if ($page == 1) echo "disabled"?>">
							<a class="page-link" href="?page=<?= $page - 1; ?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<?php
							for ($i = 1; $i <= $nbPages; $i++)
							{
						?>

						<li class="page-item <?php if ($page == $i) echo "active";?>"><a class="page-link" href="?page=<?= $i; ?>"><?= $i; ?></a></li>

						<?php
							}
						?>
						<li class="page-item <?php if ($page == $nbPages) echo "disabled"?>">
							<a class="page-link" href="?page=<?= $page + 1; ?>" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</section>
			<?php
				}
			?>
        </main>

        <footer class="mt-auto">
            <?php include 'includes/footer.php'; ?>
        </footer>

		<?php
			}
			else {
				require 'errors/error-405.php';
			}
		?>

        <script src="view/bootstrap/js/jquery.js"></script>
        <script src="view/bootstrap/js/bootstrap.js"></script>
		<script src="view/js/particles.js"></script>
		<script src="view/js/app.js"></script>
    </body>
</html>