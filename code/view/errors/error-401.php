<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../style/general.css" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
		<link rel="stylesheet" href="../style/error.css" type="text/css" />

        <title>Erreur 401 | ProgWeb</title>
    </head>

    <body>
		<div class="container">
			<div class="card mt-3">
				<div class="card-body">
					<div class="text-center">
						<span class="error mx-auto" data-text="ERROR 401">ERROR 401</span>
						<p class="lead mt-3 mb-4"><img src="../style/icons/x-circle-fill.svg" alt="" width="24" height="24"> Une authentification est nécessaire pour accéder à la ressource. <img src="../style/icons/x-circle-fill.svg" alt="" width="24" height="24"></p>
						<button type="button" class="btn btn-success"><img src="../style/icons/unlock.svg" alt="" width="24" height="24"> Se connecter</button>
						<button type="button" class="btn btn-info"><img src="../style/icons/arrow-bar-left.svg" alt="" width="24" height="24"> Retourner à l'accueil</button>
					</div>
				</div>
			</div>
		</div>

        <script src="../bootstrap/js/jquery.js"></script>
        <script src="../bootstrap/js/bootstrap.js"></script>
    </body>
</html>