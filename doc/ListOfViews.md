## Vues nécessaires au projet

### Vues globales
- index			=> page d'accueil du blog avec la liste des news
- news			=> page affichant une news sélectionnée. Les commentaires sont affichés en dessous, et un utilisateur peut ajouter un commentaire encore en dessous.
- connexion		=> permet de se connecter à la page d'administration
- admin			=> page d'administration des news ("Ajouter", "Modifier", "Supprimer")
- admin news		=> page pour administrer une news (Ajout et modification)

### Vues d'erreurs
- 404			=> erreur "Not found"
- 401			=> Identifiants invalides "Unauthorized"
- 418			=> "I'm a teapot" (bouton "Faire un café" menant vers la page d'erreur)
